var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
user = [];
connections = [];
scanId = 1;

server.listen(3000);
console.log("Server Running...");

app.get('/', function(req, res){
    res.sendFile(__dirname + "/index.html");
})

io.sockets.on('connection', function(socket) {
    connections.push(socket);
    console.log("Connected: %s socket connected", connections.length);

    //Disconnect
    socket.on('disconnect', function(socket){
        connections.splice(connections.indexOf(socket, 1));
        console.log("Disconnected: %s socket connected", connections.length);
    });
    
    //New Message
    socket.on('sendMessage', function(data){
        io.sockets.emit('newMessage',{ id: scanId, title: data, completed: false });
    })
})
